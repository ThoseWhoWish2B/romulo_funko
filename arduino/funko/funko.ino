#include "funko_display.hpp"
#include "funko_io.hpp"
#include "EEPROM.h"

//#define DEBUG
//#define PROG_EEPROM    // resets user's reconfiguration (do not uncomment)

uint8_t buffer[20] = {0};
uint8_t lamp = 0;
uint8_t temp = 0;
uint8_t index = 0;
uint8_t n_bytes = 0;

void setup()
{   
    funko_io_init(1);     // 1 = master
    funko_display_init();
    funko_keypad_init();

    Serial.begin(9600);
    Serial.println("funk start!");

    #ifdef DEBUG
        //EEPROM.write(BLINK_ADDRESS,   500&0x00FF);
        //EEPROM.write(BLINK_ADDRESS+1, 500>>8);
        
        // Serial.println(EEPROM.read(BLINK_ADDRESS));
        // Serial.println(EEPROM.read(BLINK_ADDRESS+1));
    
        // for(uint8_t index = 0; index < NUM_FUNKOS; index++)
        // {
            // //Serial.println(EEPROM.read(index));
            // Serial.println(cross_matrix[index]);
        // }
        while(1);
    
    #endif //DEBUG
    
    #ifdef PROG_EEPROM
        for(uint8_t index = 0; index < NUM_FUNKOS; index++)
        {
            EEPROM.write(index, index);
        }
    #endif //PROG_EEPROM
}

void loop()
{
    if(get_point(&p))
    {
        
        switch(curr_screen)
        {
        case MAIN_MENU:
            for(uint8_t i=0; i < SIZE_MENU; i++)
            {
                if(menu_buttons[i].pressed(p.x, p.y))
                {
                    menu_buttons[i].select();
                }
            }
        break;
        case APP:
            if(back_button.pressed(p.x, p.y))
            {
                back_button.select();
            }
        break;
        default:
            curr_screen = MAIN_MENU;
            show_main_menu();
        break;
        }
    }

    // Debug, type lamp number
    if(Serial.available())
    {
        delay(300);
        n_bytes=Serial.available();
        
        for(index = 0; index<n_bytes; index++)
        {
            temp = Serial.read();
            if(temp>'9' || temp<'0') break;
            buffer[index] = temp;
            //Serial.write(temp);
        }
        buffer[index] = 0;
        lamp = atoi(buffer);
    
        if(lamp==254) outputs_all_off();
        if(lamp==255) outputs_all_on();
        //if(lamp==256)
          //Serial.print(outputs_state);  //not working.. use printf?
        else
        {
            if(is_output_on(lamp))
                output_off(lamp);
            else
                output_on(lamp);
            
        }
        
        outputs_send_state();
        
        Serial.println(lamp);
        Serial.flush();
        lamp = 0;
        n_bytes = 0;
    }
}
