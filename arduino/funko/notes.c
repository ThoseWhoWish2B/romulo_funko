#include "stdint.h"
#include "notes.h"

uint16_t d_dorian[] = {nD3, nE3, nF3, nG3, nA3, nB3, nC4, nD4, nE4, nF4, nG4, nA4, nB4, nC5, nD5, nE5, nF5, nG5, nA5, nB5, nC6, nD6};

uint16_t ganhou[] =
{
    -1,   300
    ,nC4,  150
  //,E4,  150
    ,nG4, 150
    ,nC5, 600
    ,-1,  300
    ,0
};

uint16_t perdeu[] = 
{
    -1,   300
    ,nB4, 150
    ,nF4, 450
    ,-1,  300
    ,0
};
