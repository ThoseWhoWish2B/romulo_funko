// 74hc595 - OEB always grounded, RSTB always upped, Last bit of last SR in chain is the first lamp.
// Master device (on with display) sends stuff per serial. 

#ifndef __FUNKO_IO_HPP__
#define __FUNKO_IO_HPP__

#include <stdint.h>
#include <Arduino.h>

#define NUM_GRUPOS    8
#define NUM_OUTPUTS   136
#define NUM_FUNKOS    116
#define NUM_BYTES_OUTPUT_STATE   17
#define NUM_BYTES_BUZZER_STATE   3

#define GANHOU 0
#define PERDEU 1
#define GENIUS_BEGIN 2
#define GENIUS_END   3  

#define SR_DIN 23
#define SR_CLOCK 25
#define SR_LATCH 27

// music
#define BUZZER_PIN A8

// Lights
#define LED_R 52
#define LED_G 53
#define LED_B 50

// bug fix
//#define SOLDER_FIX
//#define N_SOLDER_JUMPS 2
//#define INV_LOG   // some modules had (non)inverted logic (!)

enum eStateBuzzer
{
     dont
    ,know
};

extern eStateBuzzer buzzer_state;
extern uint8_t cross_matrix[NUM_FUNKOS];  // conversion matrix from key to funko, key as index
extern uint8_t outputs_state[NUM_BYTES_OUTPUT_STATE];
extern uint8_t outputs_hal[NUM_OUTPUTS];
//extern uint8_t solder_jump[];

void funko_io_init(uint8_t is_master);

// alter internal representation of outputs
void outputs_all_off();
void outputs_all_on();
void output_on(uint8_t lamp);
void output_off(uint8_t lamp);
uint8_t is_output_on(uint8_t lamp);

// internal to SR
void outputs_register();

// between master and slave
void outputs_send_state();

// buzzer
void buzzer_play_seq(uint16_t *seq);
void buzzer_send_seq(uint8_t seq);
void buzzer_send_note(uint8_t note);

#endif //__FUNKO_IO_HPP__
