#include "funko_keypad.hpp"

//uint8_t col_pins[NUM_COLS] = { 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52 };
//uint8_t row_pins[ ] = { 23, 25, 27, 29, 31, 33, 35, 37 };

//uint8_t col_pins[NUM_COLS] = {23,25,27,29,31,33,35,37,39,41,43,45,47,49,51,53};
//uint8_t row_pins[NUM_ROWS]   = {38,40,42,44,46,48,50,52};

uint8_t col_pins[NUM_COLS] = {53,51,49,47,45,43,41,39,37,35,33,31,29,27,25,23};
uint8_t row_pins[NUM_ROWS]   = {50,48,46,44,42,40,38,52};

//trying to rotate matrix of 90 deg because of row/col inversion on HW
//uint8_t col_pins[NUM_ROWS] = {38,40,42,44,46,48,50,52};
//uint8_t row_pins[NUM_COLS] = {23,25,27,29,31,33,35,37,39,41,43,45,47,49,51};

void funko_keypad_init()
{
    // for(uint8_t index = 0; index<NUM_BUTTONS; index++)
    // {
        // pinMode(buttons[index], INPUT);
        // digitalWrite(buttons[index], HIGH);
    // }
    
    // init columns
    for(uint8_t index = 0; index<NUM_COLS; index++)
    {
        pinMode(col_pins[index], OUTPUT);
        digitalWrite(col_pins[index], 1);
    }
    
    // init rows
    for(uint8_t index = 0; index<NUM_ROWS; index++)
    {
        pinMode(row_pins[index], INPUT);
        digitalWrite(row_pins[index], 1);  // pull ups
    }
    
    // for(uint8_t index = 0; index<NUM_FUNKOS; index++)
    // {
        // pinMode(lamps[index], OUTPUT);
        // digitalWrite(lamps[index], HIGH);
    // }
}

void write(volatile uint8_t column)
{
    digitalWrite(col_pins[column], 0);
}

uint8_t read(volatile uint8_t row)
{
    
    if(row<NUM_ROWS)
        return digitalRead(row_pins[row]) ? 0:1;
    else
        return 0;
}

// returns the pressed key. -1 (255) if none
uint8_t get_key()
{
    uint8_t key = 0xFF;
    
    for(volatile uint8_t column = 0; column<NUM_COLS && key==0xFF; column++)
    {
        //write(column);
        //write(NUM_COLS-column);
        
        digitalWrite(col_pins[column], 0);
        //digitalWrite(col_pins[NUM_COLS-column-1], 0);

//        Serial.println();
//        Serial.print(NUM_COLS-column-1);
//        Serial.print(": ");
                
        //delayMicroseconds(10);
        //delay(1);
        
        for(volatile int8_t row = 0; row<NUM_ROWS; row++)
        {
//            Serial.print(row);
//            Serial.print(", ");
            
            if( read(row) )
            //if( read(NUM_ROWS-row-1) )
            {
                key = row*NUM_COLS+column;
                break;
            }
        }
        
        digitalWrite(col_pins[column], 1);
        //digitalWrite(col_pins[NUM_COLS-column-1], 1);
    }

    return key;
}
