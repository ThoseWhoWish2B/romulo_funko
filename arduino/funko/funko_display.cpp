#include "funko_display.hpp"
#include "funko_io.hpp"
#include "notes.h"
#include <string.h>
#include "EEPROM.h"

LCDWIKI_KBV lcd(ILI9341,A3,A2,A1,A0,A4);

// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

TSPoint p;

volatile uint8_t cron_do_count = 0;
volatile uint8_t cron_do_count_prev = 0;
volatile unsigned long cron_ti = 0;
volatile unsigned long cron_tf = 0;


volatile uint16_t blink_period = 500;

char dummy_str[2] = " ";

uint16_t colors[] =
{
     0xF800
    ,0xFFE0
    ,0x07E0
    ,0x07FF
    ,0x001F
    ,0xF800
    ,0xF81F
    ,0x8F1F
    ,0xFF18
    ,0xC7F8
    ,0x0718
    ,0xE780
    ,0x079C
    ,0xE01C
    ,0x5555
}; // color select

tButton menu_buttons[SIZE_MENU];
tButton genius_sf[20];   //genius schalt flaeche

tButton back_button(3, 0, LCD_MAXX/2-50, HEIGHT_BACK_BUTTON, 7, HEIGHT_BACK_BUTTON/2-3, show_main_menu, "<< Voltar", 1, BLUE);
tButton play_button(3, LCD_MAXY - HEIGHT_BACK_BUTTON+3, LCD_MAXX/2-47, LCD_MAXY, 7, (HEIGHT_BACK_BUTTON-3)/2-5, none, "Jogar", 2, BLUE);
tButton new_player_button(LCD_MAXX/2-47+3, LCD_MAXY - HEIGHT_BACK_BUTTON+3, LCD_MAXX-3, LCD_MAXY, 7, (HEIGHT_BACK_BUTTON-3)/2-5, none, "Novo Jogador", 2, BLUE);

char menu_strings[SIZE_MENU1][20] = 
{
    "Seq."
    ,"Aleat."
    ,"Grupos"
    ,"Teclado"
    ,"Genius"
    ,"Cron."
    ,"Reconf."
    ,"Veloc."
};

char genius_strings[20][20] =
{
    "1","2","3","4","5","6","7","8","9",
    "10","11","12","13","14","15","16","17","18","19","20"
};

void (*menu_callbacks[SIZE_MENU])(void) =
{
     sequencia_callback
    ,aleatorio_callback
    ,grupos_callback
    ,teclado_callback
    ,genius_callback
    ,cronometro_callback
    ,reconf_callback
    ,velocidade_callback
};

// void (*menu_callbacks[SIZE_MENU])(void) =
// {
    // sequencia_callback,
    // aleatorio_callback,
    // grupos_callback,
    // teclado_callback,
    // genius_callback,
    // cronometro_callback,
    // reconf_callback
// };

eCurrScreen curr_screen = MAIN_MENU;


// games
uint8_t genius_buttons[GENIUS_NUM_ELEMENTS] = 
{
    0, 1, 2, 3, 4,
   16,17,18,19,20,
   32,33,34,35,36,
   48,49,50,51,52
};

uint8_t genius_lamps[GENIUS_NUM_ELEMENTS] = 
{
   116,117,118,119,120,
   121,122,123,124,125,
   126,127,128,129,130,
   131,132,133,134,135
};

uint8_t grupos_bounds[NUM_GRUPOS] =
{
    10,13,23,45,52,67,78,NUM_FUNKOS
};

// class implementation
tButton::tButton()
{
    minx = 0;
    miny = 0;
    maxx = 0;
    maxy = 0;
    
    strxoff = 0;
    stryoff = 0;
    
    string_size = 0;
    color = BLUE;

    string = dummy_str;

    was_pressed = false;
    
    callback = none;
}

tButton::tButton
(
    uint16_t minx,
    uint16_t miny,
    uint16_t maxx,
    uint16_t maxy,
    
    uint16_t strxoff,
    uint16_t stryoff,
    
    void (*callback)(void),
    
    char *string,
    uint16_t string_size,
    uint16_t color
)
{
    this->minx = minx;
    this->miny = miny;
    this->maxx = maxx;
    this->maxy = maxy;
    
    this->strxoff = strxoff;
    this->stryoff = stryoff;
    
    this->callback = callback;
    
    this->string = string;
    this->string_size = string_size;
    this->color = color;

    this->was_pressed = false;
}

bool tButton::pressed(uint16_t x, uint16_t y)
{
    return (x>minx && x<maxx) && (y>miny && y<maxy);
};

void tButton::draw(bool selected = false)
{
    if(selected)
    {
        lcd.Set_Draw_color(YELLOW);
        lcd.Fill_Round_Rectangle(minx, miny, maxx, maxy, 5);
        //show_string(string, minx+strxoff, miny+stryoff, string_size, BLACK, YELLOW, 1);
        show_string(string, minx+strxoff, miny+stryoff, string_size, BLACK, 0xFF-color, 1);
    }
    else
    {
        lcd.Set_Draw_color(color);
        lcd.Fill_Round_Rectangle(minx, miny, maxx, maxy, 5);
        show_string(string, minx+strxoff, miny+stryoff, string_size, WHITE, color, 1);
    }
}

void tButton::select()
{
    this->draw(true);
    delay(100);
    this->draw(false);
    this->callback();
}


int get_point(TSPoint *p)
{
    digitalWrite(13, HIGH);
    *p = ts.getPoint();
    digitalWrite(13, LOW);

    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);
    
    if (p->z > MINPRESSURE && p->z < MAXPRESSURE) 
    {
        p->x = map(p->x, TS_MINX, TS_MAXX, 0, lcd.Get_Display_Width());
        p->y = map(p->y, TS_MINY, TS_MAXY, 0,lcd.Get_Display_Height());
        
        return 1;
    }
    return 0;
}

void populate_menu()
{
    for(uint8_t i = 0; i<SIZE_MENU/2; i++)
    {   
        menu_buttons[i].minx = 5;
        menu_buttons[i].miny = HEIGHT_MENU_BUTTON*i+5;
        menu_buttons[i].maxx = LCD_MAXX/2-3;
        menu_buttons[i].maxy = HEIGHT_MENU_BUTTON*(i+1);
        
        menu_buttons[i].strxoff = 15;
        menu_buttons[i].stryoff = 12;
        
        menu_buttons[i].string = menu_strings[i];
        
        menu_buttons[i].callback = menu_callbacks[i];
        menu_buttons[i].string_size = 2;
    }
    
    for(uint8_t i = SIZE_MENU/2; i<SIZE_MENU; i++)
    {   
        menu_buttons[i].minx = LCD_MAXX/2+2;
        menu_buttons[i].miny = HEIGHT_MENU_BUTTON*(i-SIZE_MENU/2)+5;
        menu_buttons[i].maxx = LCD_MAXX-5;
        menu_buttons[i].maxy = HEIGHT_MENU_BUTTON*((i-SIZE_MENU/2)+1);
        
        menu_buttons[i].strxoff = 15;
        menu_buttons[i].stryoff = 12;
        
        menu_buttons[i].string = menu_strings[i];
        menu_buttons[i].string = menu_strings[i];
        
        menu_buttons[i].callback = menu_callbacks[i];
        menu_buttons[i].string_size = 2;
    }
}

void populate_genius_sf()
{
    for(uint8_t sf=0; sf<20; sf++)
    {
        genius_sf[sf].minx = 2+(sf%4)*(57+2);
        genius_sf[sf].miny = HEIGHT_BACK_BUTTON + 8 + (sf/4)*(48+2);
        genius_sf[sf].maxx = 2+(sf%4)*(57+2)+57;
        genius_sf[sf].maxy = HEIGHT_BACK_BUTTON + 8 + (sf/4)*(48+2)+48;
        
        genius_sf[sf].strxoff = 20;
        genius_sf[sf].stryoff = 15;
        
        genius_sf[sf].string = genius_strings[sf];
        
        genius_sf[sf].callback = none;
        genius_sf[sf].string_size = 2;
        genius_sf[sf].color = MAGENTA;
    }
}

void show_string(uint8_t *str, int16_t x, int16_t y, uint8_t csize, uint16_t fc, uint16_t bc, boolean mode)
{
    lcd.Set_Text_Mode(mode);
    lcd.Set_Text_Size(csize);
    lcd.Set_Text_colour(fc);
    lcd.Set_Text_Back_colour(bc);
    lcd.Print_String(str,x,y);
}

void show_main_menu()
{    
    curr_screen = MAIN_MENU;
    lcd.Fill_Screen(BLACK);
    
    for(uint8_t i = 0; i<SIZE_MENU1; i++)
    {
        menu_buttons[i].draw(false);
    }
}

void show_genius_sf()
{
    for(uint8_t i = 0; i<20; i++)
    {
        genius_sf[i].draw(false);
    }
}

void show_app_frame()
{
    curr_screen = APP;
    lcd.Fill_Screen(BLACK);
    back_button.draw();
}

void show_game_frame()
{
    curr_screen = APP;
    lcd.Fill_Screen(BLACK);
    back_button.draw();
    play_button.draw();
    new_player_button.draw();
}

void funko_display_init()
{
    populate_menu();
    populate_genius_sf();
    
    lcd.Init_LCD();
    pinMode(XM, OUTPUT);
    pinMode(YP, OUTPUT);
    
    pinMode(13, OUTPUT);    // veio da lib do display
    
    pinMode(CRONOMETRO_PIN, INPUT);
    digitalWrite(CRONOMETRO_PIN, HIGH);
    
    blink_period  = EEPROM.read(BLINK_ADDRESS);
    blink_period += EEPROM.read(BLINK_ADDRESS+1)<<8;
    
    outputs_all_off();
    outputs_send_state();
    
    show_main_menu();
}

// callback functions
void none()
{
   
}

void sequencia_callback()
{   
    uint8_t index = 0;
    unsigned long time = 0;

    //curr_screen = SEQUENCIA;

    show_app_frame();
    
    show_string("SEQUENCIA", TITLE_XOFF, TITLE_YOFF, 2, WHITE, BLACK, 1);
    
    show_string("Os bonecos sao", 5, 60, 2, YELLOW, BLACK, 1);
    show_string("iluminados de",     5, 78, 2, YELLOW, BLACK, 1);
    show_string("forma sequencial.",  5, 96, 2, YELLOW, BLACK, 1);

    outputs_all_off();
    outputs_send_state();

    while(1)
    {
        if(get_point(&p))
        {   
            if(back_button.pressed(p.x, p.y))
            {
                outputs_all_off();
                outputs_send_state();
                
                back_button.select();
                break;
            }
        }
       
        if(millis() > (time + blink_period))
        {
            time = millis();
            
            if(index == 0)
            {
                output_off(NUM_FUNKOS-1);
            }
            else
            {
                output_off(index-1);
            }
            
            output_on(index);
            outputs_send_state();
            
            index++;
            if(index >= NUM_FUNKOS)
            {
                index = 0;
            }
        }
    }
}
void aleatorio_callback()
{
    //curr_screen = ALEATORIO;
    uint8_t index = 0;
    uint8_t prev_index = 0;
    unsigned long time = 0;

    //curr_screen = SEQUENCIA;

    show_app_frame();
    
    show_string("ALEATORIO", TITLE_XOFF, TITLE_YOFF, 2, WHITE, BLACK, 1);
    
    show_string("Os bonecos sao", 5, 60, 2, YELLOW, BLACK, 1);
    show_string("iluminados de",     5, 78, 2, YELLOW, BLACK, 1);
    show_string("forma aleatoria.",  5, 96, 2, YELLOW, BLACK, 1);

    outputs_all_off();
    outputs_send_state();

    while(1)
    {
        if(get_point(&p))
        {   
            if(back_button.pressed(p.x, p.y))
            {
                outputs_all_off();
                outputs_send_state();
                
                back_button.select();
                break;
            }
        }
       
        if(millis() > (time + blink_period))
        {
            time = millis();
            
            do
                index = random(0, NUM_FUNKOS-1);
            while(index == prev_index);
            prev_index = index;
            
            outputs_all_off();
            output_on(index);
            outputs_send_state();
        }
    }
}
void grupos_callback()
{
    //curr_screen = GRUPOS;

    volatile uint8_t key = 0xFF;
    uint8_t grupo = 0xFF;
    unsigned long time = 0;

    show_app_frame();
    
    show_string("GRUPOS", TITLE_XOFF, TITLE_YOFF, 2, WHITE, BLACK, 1);

    show_string("Pressione o botao", 5, 60, 2, YELLOW, BLACK, 1);
    show_string("de um boneco, e o", 5, 78, 2, YELLOW, BLACK, 1);
    show_string("grupo ao qual ele", 5, 96, 2, YELLOW, BLACK, 1);
    show_string("ele pertence ira", 5, 114, 2, YELLOW, BLACK, 1);
    show_string("se iluminar.", 5, 132, 2, YELLOW, BLACK, 1);

    outputs_all_off();
    outputs_send_state();

    while(1)
    {
        if(get_point(&p))
        {
            if(back_button.pressed(p.x, p.y))
            {
                outputs_all_off();
                outputs_send_state();
                
                back_button.select();
                break;
            }
        }
       
        if(millis() > (time + DEBOUNCE_DELAY))
        {
            time = millis();
            
            if(key != 0xFF)
            {
                if(key == get_key())
                {
                    // find to which group that key belongs
                    if(key < grupos_bounds[0])
                        grupo = 0;
                    else if(key < grupos_bounds[1])
                        grupo = 1;
                    else if(key < grupos_bounds[2])
                        grupo = 2;
                    else if(key < grupos_bounds[3])
                        grupo = 3;
                    else if(key < grupos_bounds[4])
                        grupo = 4;
                    else if(key < grupos_bounds[5])
                        grupo = 5;
                    else if(key < grupos_bounds[6])
                        grupo = 6;
                    else if(key < grupos_bounds[7])
                        grupo = 7;
                    else
                        grupo = 0xFF;
                    
                    // turn group on
                    if(grupo != 0xFF)
                    {
                        outputs_all_off();
                        
                        for
                        (
                             uint8_t index = (grupo==0) ? 0 : grupos_bounds[grupo-1]
                            ;index < grupos_bounds[grupo]
                            ;index++
                        )
                        {
                            output_on(cross_matrix[index]);
                        }
                        
                        outputs_send_state();
                    }
                }
                key = 0xFF;
            }
            else
            {
                key = get_key();
            }
        }
    }
}
void teclado_callback()
{
    uint8_t key = 0xFF;
    unsigned long time = 0;
    uint8_t all_on = 0;
    uint8_t buttons_offset = 175;

    tButton all_on_button(20, buttons_offset, LCD_MAXX-20, buttons_offset+2*HEIGHT_BACK_BUTTON, HEIGHT_BACK_BUTTON+12, (2*HEIGHT_BACK_BUTTON-3)/2-5, none, "TUDO ACESO", 2, BLUE);
    tButton all_off_button(20, buttons_offset+2*HEIGHT_BACK_BUTTON+5, LCD_MAXX-20, buttons_offset+4*HEIGHT_BACK_BUTTON+5, HEIGHT_BACK_BUTTON, (2*HEIGHT_BACK_BUTTON-3)/2-5, none, "TUDO APAGADO", 2, BLUE);

    show_app_frame();
    
    show_string("TECLADO", TITLE_XOFF, TITLE_YOFF, 2, WHITE, BLACK, 1);

    show_string("Pressione a caixa", 5, 60, 2, YELLOW, BLACK, 1);
    show_string("do boneco que",     5, 78, 2, YELLOW, BLACK, 1);
    show_string("deseja iluminar.",  5, 96, 2, YELLOW, BLACK, 1);

    all_on_button.draw();
    all_off_button.draw();

    outputs_all_off();
    outputs_send_state();

    while(1)
    {
        if(get_point(&p))
        {
            if(back_button.pressed(p.x, p.y))
            {
                back_button.select();
                
                outputs_all_off();
                outputs_send_state();
                
                break;
            }
            else if(all_on_button.pressed(p.x, p.y))
            {
                outputs_all_on();
                outputs_send_state();
                
                all_on_button.select();
            }
            else if(all_off_button.pressed(p.x, p.y))
            {
                outputs_all_off();
                outputs_send_state();
                
                all_off_button.select();
            }
            
            uint16_t dummy_count = 0;
            while(1)  // wait user to release button
            {
                if(!get_point(&p))
                    dummy_count++;
                else
                    dummy_count = 0;
                
                if(dummy_count>150)
                    break;
            }
        }

        #ifdef USE_KEYPAD
        if(millis() > (time + DEBOUNCE_DELAY))
        {
            time = millis();
            
            if(key != 0xFF)
            {
                if(key == get_key())
                {
                    Serial.print("key ");
                    Serial.println(key);
                    
                    if( is_output_on(cross_matrix[key]) )
                    //if( is_output_on(key) )
                    { 
                        //output_off(key); // direct
                        output_off(cross_matrix[key]); // reconfigured
                    }
                    else
                    {
                        //output_on(key); // direct
                        output_on(cross_matrix[key]); // reconfigured
                    }
                    outputs_send_state();
                }
                key = 0xFF;
                while((get_key()!=0xFF) && (millis()<(time+1000)) );
            }
            else
            {
                key = get_key();
            }
        }
        #endif
    }
    
}
void genius_callback()
{
    //curr_screen = GENIUS;
    unsigned long time = 0;
    unsigned long genius_period = 650;    // ms
    uint8_t key = 0xFF;
    
    // game
    eGeniusState genius_state = IDLE;
    uint8_t seq_num = 1;
    uint8_t seq_index = 0;
    uint8_t sequence[0xFF] = {0};
    
    //buttons
    
    
    // display
    //const char jogador_str[] = "Jogador ";
    uint16_t jogador_color[20] = {0};
    jogador_color[1] = MAGENTA;
    uint8_t points_in_line[20] = {0};
    uint8_t jogador_in_line[20] = {0};
    
    const uint8_t HEIGHT_GAME_STR = 18;
    
    char str_show[30] = {0};
    char str_buffer[10] = {0};
    
    uint8_t was_smth_pressed = 0;
    uint8_t index_of_pressed = 0;
    
    uint16_t curr_str_offset  = 19;
    uint8_t  curr_player      = 0;
    uint8_t  curr_line        = 0;

    buzzer_send_seq(GENIUS_BEGIN); // do before to leave some time between packets...

    show_game_frame();
    show_string("GENIUS", TITLE_XOFF, TITLE_YOFF, 2, WHITE, BLACK, 1);
    
    show_string("Tente repetir a", 5, 60, 2, YELLOW, BLACK, 1);
    show_string("sequencia e veja", 5, 78, 2, YELLOW, BLACK, 1);
    show_string("ate onde consegue", 5, 96, 2, YELLOW, BLACK, 1);
    show_string("chegar!", 5, 114, 2, YELLOW, BLACK, 1);

    outputs_all_off();
    outputs_send_state();
    
    randomSeed(millis());
    
    while(1)
    {
        if(get_point(&p))
        {
            if(back_button.pressed(p.x, p.y))
            {
                buzzer_send_seq(GENIUS_END); // do before to leave some time between packets...
                
                back_button.select();
                
                outputs_all_off();
                outputs_send_state();
                
                break;
            }
            
            if(genius_state == IDLE)
            {
                if(play_button.pressed(p.x, p.y))
                {   
                    play_button.select();
                    
                    if(curr_player==0)
                        curr_player++;
                    
                    curr_line++;
                    
                    jogador_in_line[curr_line] = curr_player;
                    
                    if(curr_line<14)
                    {
                        //curr_str_offset += HEIGHT_GAME_STR;
                        // strcat(str_show, jogador_str);
                        // sprintf(str_buffer, "%d", curr_player);
                        // strcat(str_show, str_buffer);
                        
                        // show_string(str_show, 3, curr_str_offset, 2, jogador_color[curr_player], BLACK, 1);
                        
                        // strcpy(str_show, "");
                        // strcpy(str_buffer, "");

                        // show genius buttons
                        lcd.Set_Draw_color(BLACK);
                        lcd.Fill_Rectangle(0, HEIGHT_BACK_BUTTON, LCD_MAXX, LCD_MAXY-HEIGHT_BACK_BUTTON);
                        show_genius_sf();
                        
                        delay(genius_period);
                        genius_state = SHOW_SEQ;                        
                    }
                }
                else if(new_player_button.pressed(p.x, p.y))
                {
                    new_player_button.select();
                    
                    curr_player++;
                    curr_line++;
                    
                    jogador_in_line[curr_line] = curr_player;
                    jogador_color[curr_player] = random(0x00C0, 0xFFFF);
                    
                    if(curr_line<14)
                    {
                        //curr_str_offset += HEIGHT_GAME_STR;
                        
                        // strcat(str_show, jogador_str);
                        // sprintf(str_buffer, "%d", curr_player);
                        // strcat(str_show, str_buffer);
                        
                        // show_string(str_show, 3, curr_str_offset, 2, jogador_color[curr_player], BLACK, 1);
                        
                        // strcpy(str_show, "");
                        // strcpy(str_buffer, "");
                        
                        // show genius buttons
                        lcd.Set_Draw_color(BLACK);
                        lcd.Fill_Rectangle(0, HEIGHT_BACK_BUTTON, LCD_MAXX, LCD_MAXY-HEIGHT_BACK_BUTTON);
                        show_genius_sf();
                        
                        delay(genius_period);
                        genius_state = SHOW_SEQ;
                    }
                }
            }
        }
        
        switch(genius_state)
        {
            case SHOW_SEQ:
                if(millis() > (time + genius_period))
                {
                    time = millis();
                    
                    if(seq_index < seq_num)
                    {
                        if(seq_index == seq_num-1)
                        {    
                            sequence[seq_index] = random(0, GENIUS_NUM_ELEMENTS-1);
                        }
                        outputs_all_off();
                        output_on( genius_lamps[sequence[seq_index]] );
                        Serial.println( genius_lamps[sequence[seq_index]] );
                        
                        outputs_send_state();
                        genius_sf[sequence[seq_index]].select();  //includes 100ms delay
                        
                        // while(1)
                        // {
                            // outputs_send_state();
                            
                            // //while((!Serial1.available() && (millis()<(time+100))) || (Serial1.available() && !(millis()<(time+100))));
                            // while( !Serial1.available() && (millis()<(time+100)) );
                            
                            // if(Serial.read() == 'A')
                                // break;
                            
                            // if(millis()>(time+100))
                                // break;
                        // }
                        
                        //buzzer_send_note(sequence[seq_index]);
                        tone(BUZZER_PIN, d_dorian[sequence[seq_index]], GENIUS_NOTE_DURATION);
                        Serial.println(sequence[seq_index]);
                        
                        seq_index++;
                    }
                    else
                    {
                        outputs_all_off();
                        outputs_send_state();
                        
                        genius_state = REC_SEQ;
                        seq_index = 0;
                    }
                }
            break;
            case REC_SEQ:
                time = millis();
                
                if(get_point(&p))
                {
                    // check all schaltflächen
                    for(uint8_t i=0; i < 20; i++)
                    {
                        if(genius_sf[i].pressed(p.x, p.y))
                        {
                            Serial.println(i); // debug
                        
                            genius_sf[i].select(); // includes 100ms delay
                        
                            outputs_all_off();
                            output_on(genius_lamps[i]);
                            outputs_send_state();
                            
                            //buzzer_send_note(i);
                            tone(BUZZER_PIN, d_dorian[i], GENIUS_NOTE_DURATION);
                            
                            if( i == sequence[seq_index] )
                            {
                                seq_index++;
                                if(seq_index >= seq_num)
                                {
                                    genius_state = SEQ_OK;
                                    seq_index = 0;
                                }
                            }
                            else
                            {
                                genius_state = SEQ_NOK;
                                seq_index = 0;
                            }
                        }
                    }
                    uint16_t dummy_count = 0;
                    while(1)  // wait user to release button
                    {
                        if(!get_point(&p))
                            dummy_count++;
                        else
                            dummy_count = 0;
                        
                        if(dummy_count>150)
                            break;
                    }
                }
                
            break;
            case SEQ_OK:
                Serial.println("OK!"); // debug
                
                buzzer_send_seq(GANHOU);  // Not actually buzz, just the LEDs
                delay(GENIUS_NOTE_DURATION); // lets last note complete
                
                // happy tune                
                buzzer_play_seq(ganhou);
                outputs_all_off();
                outputs_send_state();
                
                delay(genius_period);  // let user feel happy a bit
                
                seq_num++;
                
                genius_state = SHOW_SEQ;
            break;
            case SEQ_NOK:
                Serial.println("NOK..."); // debug
                
                buzzer_send_seq(PERDEU);  // Not actually buzz, just the LEDs
                delay(GENIUS_NOTE_DURATION); // lets last note complete
                
                // sad tune
                buzzer_play_seq(perdeu);
                delay(genius_period);  // let user feel sad a bit
                                
                // show score
                //show_game_frame();
                lcd.Set_Draw_color(BLACK);
                lcd.Fill_Rectangle(0, HEIGHT_BACK_BUTTON, LCD_MAXX, LCD_MAXY-HEIGHT_BACK_BUTTON);
                
                points_in_line[curr_line] = seq_num-1;
                
                for(uint8_t line=1; line<=curr_line; line++)
                {
                    sprintf(str_show, "Jogador %d:", jogador_in_line[line]);
                    show_string(str_show, 3, 19 + (line)*HEIGHT_GAME_STR, 2, jogador_color[jogador_in_line[line]], BLACK, 1);
                    
                    sprintf(str_show, "%d", points_in_line[line]);
                    show_string(str_show, LCD_MAXX-55, 19 + (line)*HEIGHT_GAME_STR, 2, jogador_color[jogador_in_line[line]], BLACK, 1);
                    
                    // strcpy(str_show, "");
                    // strcpy(str_buffer, "");
                }
                
                seq_num = 1;

                outputs_all_off();
                outputs_send_state();
                
                genius_state = IDLE;
            break;
        }   
    }
}
void cronometro_callback()
{
    //curr_screen = CRONOMETRO;
    
    unsigned long time = 0;
    
    // game
    uint8_t is_playing = 0;
    cron_do_count = 0;
    cron_do_count_prev = 0;
    unsigned long score = 0;
    
    // display
    const char jogador_str[] = "Jogador ";
    uint16_t jogador_color = MAGENTA;

    const uint8_t INI_STR_XOFF = 96-24;
    const uint8_t INI_STR_YOFF = 19;

    const uint8_t HEIGHT_GAME_STR = 18;
    const uint8_t WIDTH_GAME_STR  = 48; //36;
    
    char str_show[30] = {0};
    char str_buffer[10] = {0};
    
    uint16_t curr_str_xoffset  = INI_STR_XOFF;
    uint16_t curr_str_yoffset  = INI_STR_YOFF;
    uint8_t  curr_player      = 0;
    uint8_t  curr_line        = 0;
    uint8_t  curr_col         = 0;
    
    show_game_frame();
    
    show_string("CRONOMETRO", TITLE_XOFF, TITLE_YOFF, 2, WHITE, BLACK, 1);

    show_string("Veja quao rapido", 5, 60, 2, YELLOW, BLACK, 1);
    show_string("consegue pressionar", 5, 78, 2, YELLOW, BLACK, 1);
    show_string("o botao!", 5, 96, 2, YELLOW, BLACK, 1);

    outputs_all_off();
    outputs_send_state();
    
    while(1)
    {
        if(get_point(&p))
        {
            if(back_button.pressed(p.x, p.y))
            {
                outputs_all_off();
                outputs_send_state();
                
                detachInterrupt(digitalPinToInterrupt(CRONOMETRO_PIN));
                
                back_button.select();
                break;
            }
            else if(play_button.pressed(p.x, p.y) && !cron_do_count)
            {
                play_button.select();
                is_playing = 0; // only 1 if enough lines...
                
                curr_str_xoffset = INI_STR_XOFF;
                EIFR = 0x02;
                attachInterrupt(digitalPinToInterrupt(CRONOMETRO_PIN), cron_pin_isr, FALLING);
        
                if(curr_player==0)
                {
                    curr_player++;
                    lcd.Set_Draw_color(BLACK);
                    lcd.Fill_Rectangle(0, HEIGHT_BACK_BUTTON, LCD_MAXX, LCD_MAXY-HEIGHT_BACK_BUTTON);
                }
                else
                {
                    curr_line++;
                }
                
                if(curr_line<14)
                {
                    curr_str_yoffset += HEIGHT_GAME_STR;
                    strcat(str_show, jogador_str);
                    sprintf(str_buffer, "%d:", curr_player);
                    strcat(str_show, str_buffer);
                    
                    show_string(str_show, 3, curr_str_yoffset, 2, jogador_color, BLACK, 1);
                    
                    strcpy(str_show, "");
                    strcpy(str_buffer, "");
                    
                    outputs_all_off();
                    outputs_send_state();
                    
                    is_playing = 1;
                    // genius_state = SHOW_SEQ;
                }
            }
            else if(new_player_button.pressed(p.x, p.y) && !cron_do_count)
            {
                new_player_button.select();
                
                is_playing = 0; // only 1 if enough lines...
                
                if(curr_player==0)
                {
                    lcd.Set_Draw_color(BLACK);
                    lcd.Fill_Rectangle(0, HEIGHT_BACK_BUTTON, LCD_MAXX, LCD_MAXY-HEIGHT_BACK_BUTTON);
                }
                
                curr_str_xoffset = INI_STR_XOFF;
                EIFR = 0x02;
                attachInterrupt(digitalPinToInterrupt(CRONOMETRO_PIN), cron_pin_isr, FALLING);
                
                curr_player++;
                curr_line++;
                
                //jogador_color = colors[curr_player];
                
                uint16_t new_jogador_color = 0;
                uint16_t color_diff = 0;
                
                // while(color_diff < 0xC3)
                // {
                    // new_jogador_color = random(0xC3, 0xFF)<<11 + random(0xC3, 0xFF)<<5 + random(0xC3, 0xFF);
                    // color_diff = (new_jogador_color - jogador_color);
                    // color_diff = color_diff>>11 + (color_diff & 0x07E0)>>5 + (color_diff & 0x001F);
                // }

                jogador_color = random(0x00C0, 0xFFFF);
                //new_jogador_color = random(0x00C0, 0xFFFF);
                
                // jogador_color = new_jogador_color;
                //color_diff = (new_jogador_color - jogador_color);
                //color_diff = color_diff>>11 + (color_diff & 0x07E0)>>5 + (color_diff & 0x001F);
                
                if(curr_line<14)
                {
                    curr_str_yoffset += HEIGHT_GAME_STR;
                    
                    strcat(str_show, jogador_str);
                    sprintf(str_buffer, "%d:", curr_player);
                    strcat(str_show, str_buffer);
                    
                    show_string(str_show, 3, curr_str_yoffset, 2, jogador_color, BLACK, 1);
                    
                    strcpy(str_show, "");
                    strcpy(str_buffer, "");
                    
                    outputs_all_off();
                    outputs_send_state();
                    
                    is_playing = 1;
                    // genius_state = SHOW_SEQ;
                }
            }
            uint16_t dummy_count = 0;
            while(1)  // wait user to release button
            {
                if(!get_point(&p))
                    dummy_count++;
                else
                    dummy_count = 0;
                
                if(dummy_count>150)
                    break;
            }
        }
        
        if(is_playing)
        {
            if(cron_do_count && !cron_do_count_prev) // raising edge cron_do_count, began counting
            {
                EIFR = 0x02;
                attachInterrupt(digitalPinToInterrupt(CRONOMETRO_PIN), cron_pin_isr, FALLING);
                //Serial.println("count!");
            }
            else if(!cron_do_count && cron_do_count_prev) // falling edge cron_do_count, finished counting
            {
                // print result, cron_tf - cron_ti
                //Serial.println(cron_tf - cron_ti);
                
                curr_str_xoffset += WIDTH_GAME_STR;
                if(curr_str_xoffset> (LCD_MAXX-WIDTH_GAME_STR))
                {
                    curr_str_xoffset = 24;
                    curr_line++;
                    if(curr_line<=14)
                        curr_str_yoffset += HEIGHT_GAME_STR;
                    else
                    {
                        is_playing = 0;
                        break;
                    }
                }
                
                // show score
                score = (cron_tf - cron_ti)/10+1; // always rouding up
                if( score > NUM_FUNKOS )
                    score = NUM_FUNKOS;
                else if( score < 0 )
                    score = 16;     // millis rollover. cmmon, no ones gonna notice it.
                
                sprintf(str_buffer, "%3d,", score);
                show_string(str_buffer, curr_str_xoffset, curr_str_yoffset, 2, 0xFF80, BLACK, 1);
                outputs_all_off();
                for(uint8_t lamp=0; lamp<score; lamp++)
                {
                    output_on(lamp);
                }
                outputs_send_state();
                
                EIFR = 0x02;
                attachInterrupt(digitalPinToInterrupt(CRONOMETRO_PIN), cron_pin_isr, FALLING);
            }
            cron_do_count_prev = cron_do_count;
            // Serial.print(cron_do_count_prev);
            // Serial.print(" ");
            // Serial.println(cron_do_count);
        }

    }
    
}
void reconf_callback()
{
    // keypad sweeping
    uint8_t key = 0xFF;
    unsigned long time = 0;
    
    // GUI
    tButton novo_button(3, LCD_MAXY - HEIGHT_BACK_BUTTON+3, LCD_MAXX/2-47, LCD_MAXY, 7, (HEIGHT_BACK_BUTTON-3)/2-5, none, "Novo", 2, BLUE);
    char buffer_str[10] = {0};
    
    // Aplication
    static enum eReconfState
    {
         INIT
        ,WAIT_FIRST
        ,FIRST_PRESSED
        ,WAIT_SECOND
        ,SECOND_PRESSED
        ,END
    }reconf_state;
        
    static volatile uint8_t key_1 = 0xFF;
    static volatile uint8_t key_2 = 0xFF;

    static volatile uint8_t temp1 = 0xFF;
    static volatile uint8_t temp2 = 0xFF;
    
    reconf_state = INIT;
    
    while(1)
    {
        if(get_point(&p))
        {
            if(back_button.pressed(p.x, p.y))
            {   
                back_button.select();
                break;
            }
            if(reconf_state == END)
            {
                if(novo_button.pressed(p.x, p.y))
                {
                    reconf_state = INIT;
                    novo_button.select();
                }
            }
        }
        
        switch(reconf_state)
        {
            case INIT:
                key   = 0xFF;
                
                key_1 = 0xFF;
                key_2 = 0xFF;
                
                temp1 = 0xFF;
                temp2 = 0xFF;
                time = 0;
                
                outputs_all_off();
                outputs_send_state();
            
                show_app_frame();
                show_string("RECONFIG.", TITLE_XOFF, TITLE_YOFF, 2, WHITE, BLACK, 1);
            
                show_string("Pressione o botao", 5, 60, 2, YELLOW, BLACK, 1);
                show_string("do primeiro boneco", 5, 78, 2, YELLOW, BLACK, 1);
                show_string("que deseja trocar.", 5, 96, 2, YELLOW, BLACK, 1);
            
                //show instr.
                reconf_state = WAIT_FIRST;
            break;
            case WAIT_FIRST:
            
                // debounce, same as with Teclado
                if(millis() > (time + DEBOUNCE_DELAY))
                {
                    time = millis();
                    
                    if(key != 0xFF)
                    {
                        if(key == get_key())
                        {
                            key_1 = key;
                            
                            while(get_key() != 0xFF);
                            reconf_state = FIRST_PRESSED;
                        }
                    }
                    else
                    {
                        key = get_key();
                    }
                }
            break;
            case FIRST_PRESSED:
                // turn first lamp on
                output_on(key_1);
                outputs_send_state();
                
                sprintf(buffer_str, "%3d -> ", key_1+1);
                show_string(buffer_str, 25, LCD_MAXY-78, 3, MAGENTA, BLACK, 1);
                
                //reset key
                key   = 0xFF;
                
                //show instr.
                lcd.Set_Draw_color(BLACK);
                lcd.Fill_Rectangle(0, 60, LCD_MAXX, 114);
                show_string("Pressione o botao", 5, 60, 2, YELLOW, BLACK, 1);
                show_string("do segundo boneco.", 5, 78, 2, YELLOW, BLACK, 1);
                
                reconf_state = WAIT_SECOND;
            break;
            case WAIT_SECOND:
                // debounce, same as with Teclado
                if(millis() > (time + DEBOUNCE_DELAY))
                {
                    time = millis();
                    
                    if(key != 0xFF)
                    {
                        if(key == get_key())
                        {
                            key_2 = key;
                            
                            while(get_key() != 0xFF);
                            reconf_state = SECOND_PRESSED;
                        }
                    }
                    else
                    {
                        key = get_key();
                    }
                }
            break;
            case SECOND_PRESSED:
                // turn second lamp on
                output_on(key_2);
                outputs_send_state();
                
                //show change
                sprintf(buffer_str, "%10d", key_2+1);
                show_string(buffer_str, 25, LCD_MAXY-78, 3, MAGENTA, BLACK, 1);
                
                //reset key
                key   = 0xFF;
                
                //show instr.
                lcd.Set_Draw_color(BLACK);
                lcd.Fill_Rectangle(0, 60, LCD_MAXX, 114);
                show_string("Registrado!", 5, 60, 2, YELLOW, BLACK, 1);
                show_string("Agora troque os", 5, 78, 2, YELLOW, BLACK, 1);
                show_string("bonecos.", 5, 96, 2, YELLOW, BLACK, 1);
                
                // show button
                novo_button.draw();
                
                temp1 = EEPROM.read(key_1);
                temp2 = EEPROM.read(key_2);
                
                // temp1 = cross_matrix[key_1];
                // temp2 = cross_matrix[key_2];
                
                // Serial.print("tmp1 ");
                // Serial.println(temp1);
                // Serial.print("tmp2 ");
                // Serial.println(temp2);
                
                EEPROM.write(key_1, temp2);
                EEPROM.write(key_2, temp1);
                
                cross_matrix[key_1] = temp2;
                cross_matrix[key_2] = temp1;
                
                reconf_state = END;
            break;
            case END:
                
            break;
            
        }
    }
    
}
void velocidade_callback()
{
    static const uint16_t  VEL_MIN = 500;
    static const uint16_t  VEL_MAX = 5000;
    static uint16_t COLOR_STR = WHITE;
    
    uint8_t yoffset_dash = 210;
    
    tButton minus_button(5, yoffset_dash, 5+2*HEIGHT_BACK_BUTTON, yoffset_dash+HEIGHT_BACK_BUTTON, HEIGHT_BACK_BUTTON-3, (HEIGHT_BACK_BUTTON-3)/2-5, none, "-", 2, BLUE);
    tButton plus_button(LCD_MAXX - 2*HEIGHT_BACK_BUTTON - 5, yoffset_dash, LCD_MAXX-5, yoffset_dash+HEIGHT_BACK_BUTTON, HEIGHT_BACK_BUTTON-3, (HEIGHT_BACK_BUTTON-3)/2-5, none, "+", 2, BLUE);
    
    uint8_t buffer_str[20] = {0};
    
    show_app_frame();
    show_string("VELOCIDADE", TITLE_XOFF, TITLE_YOFF, 2, WHITE, BLACK, 1);
    minus_button.draw();
    plus_button.draw();
    
    show_string("Periodo em que cada",  5, 60, 2, YELLOW, BLACK, 1);
    show_string("lampada fica acesa", 5, 78, 2, YELLOW, BLACK, 1);
    show_string("para os modos",  5, 96, 2, YELLOW, BLACK, 1);
    show_string("sequencial e",  5, 114, 2, YELLOW, BLACK, 1);
    show_string("aleatorio.",  5, 132, 2, YELLOW, BLACK, 1);
    
    sprintf(buffer_str, "%2d,%1d s", blink_period/1000,(blink_period%1000)/100);
    show_string(buffer_str, HEIGHT_BACK_BUTTON+50, yoffset_dash+5, 2, WHITE, BLACK, 1);
    
    while(1)
    {
        if(get_point(&p))
        {
            if(back_button.pressed(p.x, p.y))
            {
                back_button.select();
                
                EEPROM.write(BLINK_ADDRESS,   blink_period&0x00FF);
                EEPROM.write(BLINK_ADDRESS+1, blink_period>>8);
                
                break;
            }
            else if(minus_button.pressed(p.x, p.y))
            {
                minus_button.select();
                
                COLOR_STR = WHITE;
                
                blink_period -= 100;
                if(blink_period<VEL_MIN)
                {
                    blink_period = VEL_MIN;
                    COLOR_STR = RED;
                }
                
                //Serial.println(blink_period);
                sprintf(buffer_str, "%2d,%1d s", blink_period/1000,(blink_period%1000)/100);
                
                lcd.Set_Draw_color(BLACK);
                lcd.Fill_Rectangle(5+2*HEIGHT_BACK_BUTTON, yoffset_dash, LCD_MAXX - 2*HEIGHT_BACK_BUTTON - 5, yoffset_dash+HEIGHT_BACK_BUTTON);
                show_string(buffer_str, HEIGHT_BACK_BUTTON+50, yoffset_dash+5, 2, COLOR_STR, BLACK, 1);
            }
            else if(plus_button.pressed(p.x, p.y))
            {
                plus_button.select();
                
                COLOR_STR = WHITE;
                
                blink_period += 100;
                if(blink_period>VEL_MAX)
                {
                    blink_period = VEL_MAX;
                    COLOR_STR = RED;
                }
                
                //Serial.println(blink_period);
                sprintf(buffer_str, "%2d,%1d s", blink_period/1000,(blink_period%1000)/100);
                
                lcd.Set_Draw_color(BLACK);
                lcd.Fill_Rectangle(5+2*HEIGHT_BACK_BUTTON, yoffset_dash, LCD_MAXX - 2*HEIGHT_BACK_BUTTON - 5, yoffset_dash+HEIGHT_BACK_BUTTON);
                show_string(buffer_str, HEIGHT_BACK_BUTTON+50, yoffset_dash+5, 2, COLOR_STR, BLACK, 1);
            }
        }
    }
}

void cron_pin_isr()
{
    // detachInterrupt(digitalPinToInterrupt(CRONOMETRO_PIN));
    
    if(cron_do_count)
        cron_tf = millis();
    else
        cron_ti = millis();
    
    cron_do_count = !cron_do_count;
    //Serial.println("isr!");
}
