#ifndef __FUNKO_KEYPAD_HPP__
#define __FUNKO_KEYPAD_HPP__

/*
              |
              |           0           1                    2                   ...          NUM_COLS - 1
--------------+-------------------------------------------------------------------------------------------------------
           0  |           0           1                    2                   ...          NUM_COLS - 1
           1  |           NUM_COLS    NUM_COLS + 1         NUM_COLS + 2        ...          NUM_COLS + NUM_COLS-1
           2  |         2*NUM_COLS  2*NUM_COLS + 1         2*NUM_COLS + 2      ...        2*NUM_COLS + NUM_COLS-1
         ...  |           ...
  NUM_ROWS-1  |  NUM_ROWS*NUM_COLS  NUM_ROWS*NUM_COLS+2    NUM_ROWS*NUM_COLS+3 ... NUM_ROWS*NUM_COLS + NUM_COLS-1
              |
*/

#include <stdint.h>
#include <Arduino.h>

#define NUM_ROWS 8
#define NUM_COLS 16

#define DEBOUNCE_DELAY 50

extern uint8_t col_pins[NUM_COLS];
extern uint8_t row_pins[NUM_ROWS];

//trying to rotate matrix of 90 deg because of row/col inversion on HW
//extern uint8_t col_pins[NUM_ROWS];
//extern uint8_t row_pins[NUM_COLS];

void funko_keypad_init();

uint8_t get_key();
void    write(uint8_t);
uint8_t read(uint8_t);

#endif //__FUNKO_KEYPAD_HPP__
