#ifndef __FUNKO_DISPLAY_HPP__
#define __FUNKO_DISPLAY_HPP__

#include "funko_keypad.hpp"
#include "funko_io.hpp"

#include <TouchScreen.h> //touch library
#include <LCDWIKI_GUI.h> //Core graphics library
#include <LCDWIKI_KBV.h> //Hardware-specific library

#define USE_KEYPAD

#define YP A3  // must be an analog pin, use "An" notation!
#define XM A2  // must be an analog pin, use "An" notation!
#define YM 9   // can be a digital pin
#define XP 8   // can be a digital pin

#define LCD_MAXX 240
#define LCD_MAXY 320

//param calibration from kbv
#define TS_MINX 124
#define TS_MAXX 900

#define TS_MINY 83  
#define TS_MAXY 893

#define MINPRESSURE 5 //10
#define MAXPRESSURE 1000

#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
//#define MAGENTA 0xF81F
#define MAGENTA 0xF80E
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

// negative
// #define BLACK   (~0x0000)
// #define BLUE    (~0x001F)
// #define RED     (~0xF800)
// #define GREEN   (~0x07E0)
// #define CYAN    (~0x07FF)
// #define MAGENTA (~0xF80E)
// #define YELLOW  (~0xFFE0)
// #define WHITE   (~0xFFFF)

#define SIZE_MENU1 8
#define SIZE_MENU  SIZE_MENU1

#define HEIGHT_MENU_BUTTON 79//45
#define HEIGHT_BACK_BUTTON 30

#define TITLE_XOFF  85
#define TITLE_YOFF  15

// games
#define GENIUS_NUM_ELEMENTS 20

#define CRONOMETRO_PIN 20  // PIN 44 of AVR, PD1

#define BLINK_ADDRESS 200

enum eCurrScreen
{
    MAIN_MENU,
    SEQUENCIA,
    ALEATORIO,
    GRUPOS,
    TECLADO,
    GENIUS,
    CRONOMETRO,
    RECONF,
    APP
};

class tButton
{
    public:
        uint16_t minx;
        uint16_t miny;
        uint16_t maxx;
        uint16_t maxy;
        
        uint16_t strxoff;
        uint16_t stryoff;
        
        uint16_t string_size;
        uint16_t color;

        bool was_pressed;
        
        void (*callback)(void);
        
        char *string;

        bool pressed(uint16_t x, uint16_t y);
        void draw(bool selected = false);
        void select();
        
        tButton();
        
        tButton
        (
            uint16_t minx,
            uint16_t miny,
            uint16_t maxx,
            uint16_t maxy,
            
            uint16_t strxoff,
            uint16_t stryoff,
            
            void (*callback)(void),
            
            char *string,
            uint16_t string_size,
            uint16_t color
        );
};

enum eGeniusState
{
     IDLE
    ,SHOW_SEQ
    ,REC_SEQ
    ,SEQ_OK
    ,SEQ_NOK
};

extern TSPoint p;

extern volatile uint8_t cron_do_count;
extern volatile uint8_t cron_do_count_prev;
extern volatile unsigned long cron_ti;
extern volatile unsigned long cron_tf;

extern volatile uint16_t blink_period;

extern char dummy_str[2];

extern eCurrScreen curr_screen;

extern LCDWIKI_KBV lcd;
extern TouchScreen ts;
extern uint16_t colors[];

extern tButton back_button;
extern tButton new_player_button;
extern tButton play_button;
extern tButton menu_buttons[SIZE_MENU];
extern tButton genius_sf[20];   //genius schaltflaeche
extern char menu_strings[SIZE_MENU1][20];
extern uint8_t grupos_bounds[NUM_GRUPOS];
//extern TSPoint p;

//games
extern uint8_t genius_conv_table[GENIUS_NUM_ELEMENTS];

int get_point(TSPoint *p);

void populate_menu();
void populate_genius_sf();
void show_string(uint8_t *str,int16_t x,int16_t y,uint8_t csize,uint16_t fc, uint16_t bc,boolean mode);
void show_main_menu();
void show_app_frame();
void show_game_frame();
void show_genius_sf();
void funko_display_init();

void cron_pin_isr();

// callbacks
void none();
void sequencia_callback();
void aleatorio_callback();
void grupos_callback();
void teclado_callback();
void genius_callback();
void cronometro_callback();
void reconf_callback();
void velocidade_callback();



#endif //__FUNKO_DISPLAY_HPP__
