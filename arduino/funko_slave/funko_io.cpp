#include "funko_io.hpp"
#include "EEPROM.h"
#include "notes.h"

eStateBuzzer buzzer_state = dont;
uint8_t outputs_state[NUM_BYTES_OUTPUT_STATE] = {0}; // internal, perfect representation of the outputs
uint8_t cross_matrix[NUM_FUNKOS];         // user swapping layer

//uint8_t outputs_hal[NUM_OUTPUTS] =        // HW Abstraction Layer
//{
//     1    ,2    ,3     ,4    ,5    ,6    ,14   ,15   // 7
//    ,7    ,8    ,9     ,10   ,11   ,12   ,13   ,0    // 15
//    ,49   ,50   ,51    ,52   ,54   ,53   ,55   ,56   // 23
//    ,41   ,42   ,43    ,44   ,45   ,46   ,47   ,48   // 31
//    ,32   ,34   ,35    ,36   ,37   ,38   ,39   ,40   // 39
//    ,24   ,25   ,26    ,27   ,28   ,29   ,30   ,31   // 47
//    ,16   ,17   ,18    ,19   ,20   ,21   ,22   ,23   // 55
//    ,87   ,88   ,89    ,90   ,91   ,92   ,93   ,94   // 63
//    ,79   ,80   ,81    ,82   ,83   ,84   ,85   ,86   // 71
//    ,71   ,72   ,73    ,74   ,75   ,76   ,77   ,78   // 79
//    ,65   ,66   ,67    ,68   ,69   ,70   ,33   ,71   // 87
//    ,57   ,58   ,59    ,60   ,61   ,62   ,63   ,64   // 95
//    ,129  ,130  ,131   ,132  ,133  ,134  ,135  ,255  // 103   // 57 does not exist
//    ,121  ,122  ,123   ,124  ,125  ,126  ,127  ,128  // 111
//    ,113  ,114  ,115   ,117  ,116  ,118  ,119  ,120  // 119
//    ,103  ,105  ,107   ,108  ,109  ,110  ,111  ,112  // 127
//    ,95   ,96   ,97    ,98   ,99   ,100  ,101  ,102  // 135
//};


uint8_t outputs_hal[NUM_OUTPUTS] =        // HW Abstraction Layer
{
    1  ,
    2 ,
    3 ,
    4 ,
    5 ,
    6 ,
    14  ,
    15  ,
    7 ,
    8 ,
    9 ,
    10  ,
    11  ,
    12  ,
    13  ,
    0 ,
    49  ,
    50  ,
    51  ,
    52  ,
    54  ,
    53  ,
    55  ,
    56  ,
    41  ,
    42  ,
    43  ,
    44  ,
    45  ,
    46  ,
    47  ,
    48  ,
    32  ,
    34  ,
    35  ,
    36  ,
    37  ,
    38  ,
    39  ,
    40  ,
    24  ,
    25  ,
    26  ,
    27  ,
    28  ,
    29  ,
    30  ,
    31  ,
    16  ,
    17  ,
    18  ,
    19  ,
    20  ,
    21  ,
    22  ,
    23  ,
    88  ,
    89  ,
    90  ,
    91  ,
    92  ,
    93  ,
    94  ,
    95  ,
    80  ,
    81  ,
    82  ,
    83  ,
    84  ,
    85  ,
    86  ,
    87  ,
    72  ,
    73  ,
    74  ,
    75  ,
    76  ,
    77  ,
    78  ,
    79  ,
    65  ,
    66  ,
    67  ,
    68  ,
    69  ,
    70  ,
    33  ,
    71  ,
    57  ,
    58  ,
    59  ,
    60  ,
    61  ,
    62  ,
    63  ,
    64  ,
    129 ,
    130 ,
    131 ,
    132 ,
    133 ,
    134 ,
    135 ,
    255 ,
    121 ,
    122 ,
    123 ,
    124 ,
    125 ,
    126 ,
    127 ,
    128 ,
    112 ,
    113 ,
    114 ,
    117 ,
    116 ,
    118 ,
    119 ,
    120 ,
    104 ,
    105 ,
    106 ,
    107 ,
    108 ,
    109 ,
    110 ,
    111 ,
    96  ,
    97  ,
    98  ,
    99  ,
    100 ,
    101 ,
    102 ,
    103 
};

//uint8_t outputs_hal[NUM_OUTPUTS] =        // HW Abstraction Layer
//{
//    0 , 1 , 2 , 3 , 4 , 5 , 6 , 7
//  , 8 , 9 , 10  , 11  , 12  , 13  , 14  , 15
//  , 16  , 17  , 18  , 19  , 20  , 21  , 22  , 23
//  , 24  , 25  , 26  , 27  , 28  , 29  , 30  , 31
//  , 32  , 33  , 34  , 35  , 36  , 37  , 38  , 39
//  , 40  , 41  , 42  , 43  , 44  , 45  , 46  , 47
//  , 48  , 49  , 50  , 51  , 52  , 53  , 54  , 55
//  , 56  , 57  , 58  , 59  , 60  , 61  , 62  , 63
//  , 64  , 65  , 66  , 67  , 68  , 69  , 70  , 71
//  , 72  , 73  , 74  , 75  , 76  , 77  , 78  , 79
//  , 80  , 81  , 82  , 83  , 84  , 85  , 86  , 87
//  , 88  , 89  , 90  , 91  , 92  , 93  , 94  , 95
//  , 96  , 97  , 98  , 99  , 100 , 101 , 102 , 103
//  , 104 , 105 , 106 , 107 , 108 , 109 , 110 , 111
//  , 112 , 113 , 114 , 115 , 116 , 117 , 118 , 119
//  , 120 , 121 , 122 , 123 , 124 , 125 , 126 , 127
//  , 128 , 129 , 130 , 131 , 132 , 133 , 134 , 135
//};

// next table contains numbers indexed from 1!!!!!
//uint8_t outputs_hal[NUM_OUTPUTS] =        // HW Abstraction Layer
//{
//     2 ,3 ,4 ,3 ,5 ,6 ,7 ,15
//    ,16,8 ,9 ,10,11,12,13,14
//    ,1 ,49,50,51,52,54,53,55
//    ,56,41,42,43,44,45,46,47
//    ,48,33,35,36,37,38,39,40
//    ,25,26,27,28,29,30,31,32
//    ,17,18,19,20,21,22,23,24
//    ,90,91,92,93,94,95,96,97
//    ,82,83,84,85,86,87,88,89
//    ,74,75,76,77,78,79,80,81
//    ,67,68,69,70,71,72,34,73
//    ,59,60,61,62,63,64,65,66
//    ,130,131,132,133,134,135
//    ,136,137,122,123,124,125
//    ,126,127,128,129,114,115
//    ,116,118,117,119,120,121
//    ,106,107,108,109,110,111
//    ,112,113,98,99,100,101
//    ,102,103,104,105
//};

//uint8_t outputs_hal[NUM_OUTPUTS] =        // HW Abstraction Layer
//{
//    16,0,1,3,2,4,5,6
//    ,9,10,11,12,13,14,15,7
//    ,8,48,49,50,51,52,53,54
//    ,55,40,41,42,43,44,45,46
//    ,47,33,86,34,35,36,37,38
//    ,39,25,26,27,28,29,30,31
//    ,32,17,18,19,20,22,21,23
//    ,24,88,89,90,91,92,93,94
//    ,95,80,81,82,83,84,85,87
//    ,72,73,74,75,76,77,78,79,64
//    ,65,66,67,68,69,70,71,56
//    ,57,58,59,60,61,62,63,128
//    ,129,130,131,132,133,134,135,120
//    ,121,122,123,124,125,126,127,112
//    ,113,114,116,115,117,118,119,104
//    ,105,106,107,108,109,110,111,96
//    ,97,98,99,100,101,102,103
//};

//uint8_t outputs_hal[NUM_OUTPUTS] =        // HW Abstraction Layer
//{
//    1,2,3,4,5,6,14,15
//    ,7,8,9,10,11,12,13,0
//    ,48,49,50,51,53,52,54,55
//    ,40,41,42,43,44,45,46,47
//    ,32,34,35,36,37,38,39,24
//    ,25,26,27,28,29,30,31,16
//    ,17,18,19,20,21,22,23,89
//    ,90,91,92,93,94,95,96,81
//    ,82,83,84,85,86,87,88,73
//    ,74,75,76,77,78,79,80,66
//    ,67,68,69,70,71,33,72,58
//    ,59,60,61,62,63,64,65,129
//    ,130,131,132,133,134,135,136,121
//    ,122,123,124,125,126,127,128,113
//    ,114,115,117,116,118,119,120,105
//    ,106,107,108,109,110,111,112,97
//    ,98,99,100,101,102,103,104,135
//};

//uint8_t solder_jump[N_SOLDER_JUMPS] = {23, 56}; //just examples!

void funko_io_init(uint8_t is_master)
{
    Serial1.setTimeout(300);
    Serial1.begin(9600);
    
    if(is_master)
    {
        // fetch dictionary key->funko from eeprom
        for(uint8_t key=0; key<NUM_FUNKOS; key++)
        {
            cross_matrix[key] = EEPROM.read(key);
        }
        
        pinMode(BUZZER_PIN, OUTPUT);
        digitalWrite(BUZZER_PIN, HIGH);
        
        outputs_all_off();
        outputs_send_state();
    }
    else
    {
        //digitalWrite(LED_B, HIGH);  // only turn on in Genius
        
        pinMode(SR_DIN, OUTPUT);
        pinMode(SR_CLOCK, OUTPUT);
        pinMode(SR_LATCH, OUTPUT);

        pinMode(LED_R, OUTPUT);
        pinMode(LED_G, OUTPUT);
        pinMode(LED_B, OUTPUT);
    }
}

void outputs_all_off()
{
    for(uint8_t byte_iter=0; byte_iter<NUM_BYTES_OUTPUT_STATE; byte_iter++)
    {
        outputs_state[byte_iter] = 0xFF; // inverted logic
    }
}
void outputs_all_on()
{
    for(uint8_t byte_iter=0; byte_iter<NUM_BYTES_OUTPUT_STATE; byte_iter++)
    {
        outputs_state[byte_iter] = 0x00; // inverted logic
    }
}
void output_on(uint8_t lamp)
{
    uint8_t byte_iter = lamp/8;
    uint8_t position_lamp_in_byte = lamp%8;
    
    //outputs_state[byte_iter] |= 1 << position_lamp_in_byte;
    outputs_state[byte_iter] &= ~(1 << position_lamp_in_byte); // Inverted logic
}
void output_off(uint8_t lamp)
{
    uint8_t byte_iter = lamp/8;
    uint8_t position_lamp_in_byte = lamp%8;
    
    //outputs_state[byte_iter] &= ~(1 << position_lamp_in_byte);
    outputs_state[byte_iter] |= 1 << position_lamp_in_byte; // Inverted logic
}

void outputs_register()
{
//    Serial.println();
//    Serial.println("HAL");
  
    int DELAY = 100;  // in us
    digitalWrite(SR_LATCH, 0);
    delayMicroseconds(DELAY);
    
    //debug
    // for(uint8_t byte_iter=0; byte_iter<NUM_BYTES_OUTPUT_STATE; byte_iter++)
    // {
        // outputs_state[byte_iter] = 0x0F;
    // }
    //\debug
    
#if defined(SOLDER_FIX)
    // shift all state bytes, use SR's DOUT
    for(uint8_t byte_iter=0; byte_iter<NUM_BYTES_OUTPUT_STATE; byte_iter++)
    {
        for(uint8_t bit_iter=0; bit_iter<8; bit_iter++)
        {
            if
            (
                (byte_iter+bit_iter)>=solder_jump[0] ||
                (byte_iter+bit_iter)>=solder_jump[1]
            )
            {
                bit_iter++;
            }
            
            if(bit_iter>=8) break;

            digitalWrite(SR_DIN, outputs_state[byte_iter]&(1<<bit_iter));  // output data
            
            // cycle the clock
            digitalWrite(SR_CLOCK, 0);
            delayMicroseconds(DELAY);
            digitalWrite(SR_CLOCK, 1);
            delayMicroseconds(DELAY);
        }
    }
    for(uint8_t sldr_jmp; sldr_jmp<N_SOLDER_JUMPS; sldr_jmp++)
    {
        digitalWrite(SR_DIN, outputs_state[solder_jump[sldr_jmp]%8] & ( 1 << (solder_jump[sldr_jmp]/8)) );  // output data
        
        // cycle the clock
        digitalWrite(SR_CLOCK, 0);
        delayMicroseconds(DELAY);
        digitalWrite(SR_CLOCK, 1);
        delayMicroseconds(DELAY);
    }
#elif defined(INV_LOG)
    Serial.println("INV_LOG");
    // shift all state bytes, use SR's DOUT
    for(uint8_t byte_iter=0; byte_iter<NUM_BYTES_OUTPUT_STATE; byte_iter++)
    {
        for(uint8_t bit_iter=0; bit_iter<8; bit_iter++)
        {
            // output data
            //if((byte_iter==0)||(byte_iter==1)||(byte_iter==6))
            //{
                //digitalWrite(SR_DIN, !outputs_state[outputs_hal[byte_iter]]&(0x80>>bit_iter));   // with HAL
                //digitalWrite(SR_DIN, !outputs_state[outputs_hal[byte_iter]]&(0x01>>bit_iter));   // with HAL
            //}
            //else
            {
                //digitalWrite(SR_DIN, outputs_state[outputs_hal[byte_iter]]&(0x80>>bit_iter));   // with HAL
                digitalWrite(SR_DIN, !outputs_state[outputs_hal[byte_iter]]&(0x01<<bit_iter));   // with HAL
            }
            
            // cycle the clock
            digitalWrite(SR_CLOCK, 0);
            delayMicroseconds(DELAY);
            digitalWrite(SR_CLOCK, 1);
            delayMicroseconds(DELAY);
        }
    }
#else
    // shift all state bytes, use SR's DOUT
    for(uint16_t byte_iter=0; byte_iter<NUM_BYTES_OUTPUT_STATE; byte_iter++)
    {
        for(uint16_t bit_iter=0; bit_iter<8; bit_iter++)
        {
            static uint16_t actual_lamp = 0;
            //uint16_t actual_lamp = 0;
            
            // output data
            //digitalWrite(SR_DIN, outputs_state[byte_iter]&(1<<bit_iter));  // wrong endianness?
            //digitalWrite(SR_DIN, outputs_state[byte_iter]&(0x80>>bit_iter));
            //digitalWrite(SR_DIN, outputs_state[byte_iter]&(0x01<<bit_iter));
            //digitalWrite(SR_DIN, outputs_state[outputs_hal[byte_iter]]&(0x01<<bit_iter));   // with HAL

            //if indexing from 0:
            actual_lamp = outputs_hal[byte_iter*8+bit_iter]; //cycle trough all SRs, through all bits, i.e. through all lamps

            //if indexing from 1:
            //actual_lamp = outputs_hal[(byte_iter*8+bit_iter)-1]; //cycle trough all SRs, through all bits, i.e. through all lamps
            
            digitalWrite(SR_DIN, outputs_state[actual_lamp/8]&(0x01<<(actual_lamp%8)));   // with HAL
            
            //Serial.print(actual_lamp);
//            Serial.print(byte_iter*8+bit_iter);
//            Serial.print("=");
//            Serial.write((outputs_state[actual_lamp/8]&(0x01<<(actual_lamp%8))) ? '1':'0' );
//            Serial.print("\n");
            
            // cycle the clock
            digitalWrite(SR_CLOCK, 0);
            delayMicroseconds(DELAY);
            digitalWrite(SR_CLOCK, 1);
            delayMicroseconds(DELAY);
        }
    }
#endif
    
    // Register all the serial data sent (OE is always on on the chips)
    digitalWrite(SR_LATCH, 1);
    delayMicroseconds(2);
}

// Serial functions
void outputs_send_state()
{
    Serial1.write('L');
    
    for(uint8_t byte_iter=0; byte_iter<NUM_BYTES_OUTPUT_STATE; byte_iter++)
    {
        Serial1.write(outputs_state[byte_iter]);

    }
}

uint8_t is_output_on(uint8_t lamp)
{
    uint8_t byte_iter = lamp/8;
    uint8_t position_lamp_in_byte = lamp%8;
    
    //outputs_state[byte_iter] |= 1 << position_lamp_in_byte;
    return ( ~outputs_state[byte_iter] & (1 << position_lamp_in_byte) ); // Inverted logic
}

// buzzer
void buzzer_play_seq(uint16_t *seq)
{
    unsigned long time = 0;
    while(*seq)
    {        
        if(*seq == -1)
        {
            noTone(BUZZER_PIN);
        }
        else
        {
            tone(BUZZER_PIN,*seq);
            Serial.println(*seq);
            
            // LEDs (gambi)
            if(seq == ganhou)
                digitalWrite(LED_G, !digitalRead(LED_G));
            else if(seq == ganhou)
                digitalWrite(LED_R, !digitalRead(LED_R));
        }
        
        delay(*(seq+1));
        seq += 2;
    }
    noTone(BUZZER_PIN);
    
    digitalWrite(LED_R, 0);
    digitalWrite(LED_G, 0);
}

void buzzer_send_seq(uint8_t seq)
{
    Serial1.write('B');
    Serial1.write('S');
    Serial1.write(seq);
}

void buzzer_send_note(uint8_t note)
{
    Serial1.write('B');
    Serial1.write('N');
    Serial1.write(note);
}
