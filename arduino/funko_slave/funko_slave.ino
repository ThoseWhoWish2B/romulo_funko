#include "funko_io.hpp"
#include "notes.h"

void setup()
{   
    //debug
    #ifdef DEBUG
      Serial.begin(115200);
    #endif
    
    funko_io_init(0);
    // for(int i=0; i<NUM_BYTES_OUTPUT_STATE; i++)
    // {
        // outputs_state[i] = 0xFF;
        
        // if(i==15) outputs_state[i] = 0xFE;
    // }
    // outputs_register();

    // while(1);

//    buzzer_play_seq(ganhou);
//    //tone(BUZZER_PIN, D3, 600);
//
//    delay(1000);
//
//    buzzer_play_seq(perdeu);
//    while(1);
//
//    tone(BUZZER_PIN, D3, 600);
//    delay(600);
//    tone(BUZZER_PIN, FS3, 600);
//    delay(600);
//    tone(BUZZER_PIN, CS4, 600);
//    delay(600);
//    tone(BUZZER_PIN, E4, 600);
//    delay(600);
//
//    for(uint8_t i=0; i<8; i++)
//    {
//        tone(BUZZER_PIN, d_dorian[i], 300);
//        delay(400);
//    }
//    while(1)
//    {
//        tone(BUZZER_PIN, d_dorian[random(0,19)], 300);
//        delay(400);
//    }

// while(!Serial1.available());
// delay(120);
}

uint8_t num_bytes_recvd = 0;

void loop()
{
    while(!Serial1.available());
    delay(30);
    
    num_bytes_recvd = Serial1.available();
    
//    Serial.println(num_bytes_recvd);

    //debug
//    if(num_bytes_recvd)
//    {
//        Serial.write(num_bytes_recvd);
//        
//        for(uint8_t byte=0; byte<num_bytes_recvd; byte++)
//        {
//            //Serial.println(Serial1.read());
//            Serial1.read();
//        }
//    }
    
    // packet for lamps
    if(num_bytes_recvd == (NUM_BYTES_OUTPUT_STATE+1) )
    {
        //Serial.println(num_bytes_recvd);  // debug
        
        if(Serial1.read()=='L') // lamps
        {
            //Serial.println('L');
          
            Serial1.write('A'); // ack
            
            for(uint8_t byte_iter=0; byte_iter<NUM_BYTES_OUTPUT_STATE; byte_iter++)
            {
                outputs_state[byte_iter] = Serial1.read();
                #ifdef DEBUG
                  Serial.write(outputs_state[byte_iter]);
                #endif
            }
            outputs_register();
            
            #ifdef DEBUG
              Serial.println();
            #endif
        }
    }
    // packet for buzzer (now LEDs...)
    else if(num_bytes_recvd == NUM_BYTES_BUZZER_STATE ) // buzzer enum + header
    {
        #ifdef DEBUG
          Serial.println(num_bytes_recvd);  // debug
        #endif
        
        if(Serial1.read() == 'B') // buzzer
        {
            #ifdef DEBUG
              Serial.println('B');
            #endif
            
            Serial1.write('A'); // ack

            uint8_t next_byte = Serial1.read();
            
            if(next_byte == 'S') // sequence
            {
                #ifdef DEBUG
                  Serial.println('S');
                #endif

                next_byte = Serial1.read();
                
                if(next_byte == GANHOU)
                {
                    #ifdef DEBUG
                      Serial.println("ganhou!");
                    #endif
                    
                    //buzzer_play_seq(ganhou); // buzzer went to main uC
                    
                    digitalWrite(LED_B, LOW);
                    for(uint8_t cnt=0; cnt<5; cnt++)
                    {
                        delay(100);
                        digitalWrite(LED_G, HIGH);
                        delay(200);
                        digitalWrite(LED_G, LOW);
                    }
                    digitalWrite(LED_B, HIGH);
                }
                else if(next_byte == PERDEU)
                {
                    #ifdef DEBUG
                      Serial.println("perdeu...");
                    #endif
                    
                    //buzzer_play_seq(perdeu); // buzzer went to main uC
                    
                    digitalWrite(LED_B, LOW);
                    for(uint8_t cnt=0; cnt<5; cnt++)
                    {
                        delay(100);
                        digitalWrite(LED_R, HIGH);
                        delay(200);
                        digitalWrite(LED_R, LOW);
                    }
                    digitalWrite(LED_B, HIGH);
                }
                else if(next_byte == GENIUS_BEGIN)
                {
                    digitalWrite(LED_B, HIGH);
                }
                else if(next_byte == GENIUS_END)
                {
                    digitalWrite(LED_B, LOW);
                }
            }
            else if(next_byte == 'N') // single notes
            {
                #ifdef DEBUG
                  Serial.println('N');
                #endif
                
                uint8_t note_number = Serial1.read();
                tone(BUZZER_PIN, d_dorian[note_number], GENIUS_NOTE_DURATION);
            }
            else
            {
                #ifdef DEBUG
                  Serial.println(next_byte);
                #endif
            }
        }
    }
    else
    {
        #ifdef DEBUG
          Serial.println('N');
        #endif
        Serial1.write('N'); // nack
      
        #ifdef DEBUG
          Serial.print(num_bytes_recvd);  // debug
        #endif
        for(uint8_t byte_iter=0; byte_iter<num_bytes_recvd; byte_iter++)
            Serial1.read();
    }

    num_bytes_recvd = 0;
}
