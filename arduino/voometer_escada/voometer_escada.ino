#include "math.h"
#include "colors.h"

//#define PNEU
//#define FRIGO_CIMA
//#define FRIGO_BAIXO

#if defined( PNEU )
  #define OFFSET 220
  #define MAX (780-OFFSET)
#else // defined( FRIGO_CIMA )
  #define FC_OFFSET 340//400
  #define FC_MAX (520-FC_OFFSET)//(650-FC_OFFSET)//(780-FC_OFFSET)
//#elif defined( FRIGO_BAIXO )
  #define FB_OFFSET 340//400
  #define FB_MAX (520-FB_OFFSET)//(650-FB_OFFSET)//(730-FB_OFFSET)
  
  #define OFFSET (FB_OFFSET+FC_OFFSET)
  #define MAX (FB_MAX+FC_MAX)
#endif

#define MIC1_PIN A0 // FB ou PNEU
#define MIC2_PIN A1

#define PWM_R 5
#define PWM_G 6
#define PWM_B 9

int32_t mic1_raw = 0;
int32_t mic2_raw = 0;

float mic1_avg = 0;
float mic2_avg = 0;

int16_t sum_raw = 0;

uint16_t pwm_r = 0;
uint16_t pwm_g = 0;
uint16_t pwm_b = 0;

uint16_t count = 0;

uint16_t index = 0;

void setup()
{
    Serial.begin(115200);
    
    pinMode(PWM_R, OUTPUT);
    pinMode(PWM_G, OUTPUT);
    pinMode(PWM_B, OUTPUT);
    
    pinMode(MIC1_PIN, INPUT);
    pinMode(MIC2_PIN, INPUT);
}

void loop()
{
    mic1_raw = analogRead(MIC1_PIN);
    mic2_raw = analogRead(MIC2_PIN);
    
    #if defined(PNEU)
        sum_raw = (mic1_raw)-OFFSET;
    #else
        sum_raw = (mic1_raw + mic2_raw)-OFFSET;
    #endif
    
    //Serial.println(mic1_raw);
    
    if(sum_raw<0)
        sum_raw = 0;
    else if(sum_raw>MAX)
        sum_raw = MAX;
    
    index = map(sum_raw, 0,MAX, 0,249);
    
    pwm_r = colors[index][0];
    pwm_g = colors[index][1];
    pwm_b = colors[index][2];

    Serial.print(mic1_raw);
    Serial.print(", ");
    Serial.print(mic2_raw);
    Serial.print("     ");

    Serial.print(pwm_b);
    Serial.print(", ");
    Serial.print(pwm_g);
    Serial.print(", ");
    Serial.println(pwm_r);

    analogWrite(PWM_B, pwm_b);
    analogWrite(PWM_G, pwm_g);
    analogWrite(PWM_R, pwm_r);

    // digitalWrite(PWM_B, 255);
    // analogWrite(PWM_G, 255);
    // analogWrite(PWM_R, 255);

    // Serial.print(mic1_avg);
    // Serial.print(", ");
    // Serial.println(mic1_avg);
}
